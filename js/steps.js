var DAYS = ["SUNDAY", "MONDAY", "TUESDAY", "WEDNESDAY", "THURSDAY", "FRIDAY", "SATURDAY"];

function dataTransform(data){

  var dataMap = {}
    data.forEach(function(d){

      var day = DAYS[new Date(d.logDate).getDay()];

      if(dataMap[day]){
        // add value for respective day
        dataMap[day][0]+=1;
        dataMap[day][1]+=parseInt(d.SumOfsteps);
      } else{
        // initialize
        dataMap[day] = [1, parseInt(d.SumOfsteps)];
      }

    });

// now we have a map of day => [occurence, totalSteps]
// find avg for each day

    var newData = [];
    for(var i=0; i<DAYS.length; i++){
        var vals = dataMap[DAYS[i]];
        //dataMap[DAYS[i]] = vals[1]/vals[0];
        var obj = {
                    "day" : DAYS[i],
                    "avg" : vals[1] / vals[0]
                  };

        newData.push(obj);
    }
    delete dataMap;
    console.log(newData);
    return newData;
};

var data = d3.csv("sample-weekly-viz.csv", function(error, data){

  if(error){
    alert("no data");
  } else{

    var x = d3.scale.ordinal().rangeRoundBands([0, 700], 0.1);
    var y = d3.scale.linear().range([800, 0]);

    var xAxis = d3.svg.axis().scale(x).orient("bottom");
    var yAxis = d3.svg.axis().scale(y).orient("left");
    var goalAxis = d3.svg.axis();

    var svg = d3.select("#chart").append("svg")
                .attr("width", 1200)
                .attr("height", 900)
                .append("g")
                .attr("transform", "translate( 90, 10)");

    // transform data to hashmap of day - avg.
    var newData = dataTransform(data);

    x.domain(newData.map(function(d){ return d.day;}));
    console.log(d3.min(newData));
    y.domain([d3.min(newData, function(d){ return d.avg-40;}), d3.max(newData, function(d){ return d.avg+500;})]);
    
    // create the x axis
    svg.append("g")
      .attr("class", "x-axis")
      .attr("transform", "translate(0, 800)")
      .call(xAxis);

    // create the y axis
    svg.append("g")
      .attr("class", "y-axis")
      .call(yAxis)
      .append("text") // add text to y axis
      .attr("transform", "rotate(-90)") // the the text vertical
      .attr("y", -40)
      .attr("dy", "1em")
      .attr("text-anchor", "end")
      .text("Average");

    // create the bars from data
    svg.selectAll(".bar")
      .data(newData)
      .enter().append("rect")
      .attr("class", "bar")
      .attr("transform", "translate(20,0)")
      .attr("x", function(d) {return x(d.day);})
      .attr("width", 30)  // create width for bar
      .attr("y", function(d){ return y(d.avg);})
      .attr("height", function(d){ return 800 - y(d.avg);}); // create height for bar (dynamic based on data)

    // create the goal attainment axis

    svg.append("path")
      .attr("class", "goal")
      .attr("d", "M 0 0 L 800 0")
      .attr("stroke", "green")
      .attr("transform", "translate(0,200)");
  }
});
