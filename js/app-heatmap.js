
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

var startDate = new Date(2014, 09, 01);

var startTs = startDate.getTime()/1000;
var step = 86400; // time diff b/w two days
var dataTicks = {};
dataTicks[startTs] = 20;

var ts = startTs+step;
// build the json
for(var i=0; i<120; i++){
  dataTicks[ts] = getRandomInt(4, 20);
  ts+=step;
}

console.log(dataTicks);

var cal = new CalHeatMap();
cal.init({
  itemSelector: "#cal-heatmap",
  domain: "month",
  subDomain: "day",
  cellSize: 20,
  cellPadding: 4,
  subDomainTextFormat: "%d",
  range: 4,
  displayLegend: true,
  tooltip: true,
  data: dataTicks,
  legend: [5, 10, 15],
  legendColors: ["#efefef", "#0F4D0B"],

});


